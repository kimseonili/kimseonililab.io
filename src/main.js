import Vue from 'vue'
import App from './App.vue'
import 'vuetify/dist/vuetify.min.css'
import Vuetify from 'vuetify'
import VueRouter from './router'

Vue.use(VueRouter)
Vue.use(Vuetify)
Vue.config.productionTip = false

// var history = require('connect-history-api-fallback');
// Vue.use(history())

new Vue({
  router: VueRouter,
  render: h => h(App)
}).$mount('#app')
