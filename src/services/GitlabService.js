import axios from 'axios'

const baseURL = "https://gitlab.com/api/v4/";
const gitLab = axios.create({
  baseURL: baseURL,
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    "PRIVATE-TOKEN": "QDG85E61ZSx5cpVQXuy4"
  }
});

export default {
  getProjects() {
    return gitLab.get("users/kimseonili/projects");
  },
  async getAlgorithmFiles() {
    let files = [];
    let page = 1;
    let tmp
    while (page) {
      tmp = await gitLab.get("projects/15513334/repository/tree?path=Baekjoon%2fsrc&page=" + page++);
      if (tmp.data.length == 0) {
        return files;
      }
      files = files.concat(tmp.data);
    }

    return;
  },
  getKoreanTime(curDate){
    let d = curDate;
    d.setUTCHours(d.getUTCHours() + 9);
    let year = d.getUTCFullYear();
    let month = d.getUTCMonth() + 1;
    let date = d.getUTCDate();
    let dateString = year + "-" + month + "-" + (date - 1) + "T15:00:00";
    return dateString;
  },

  getTodayCommitAlgorithm() {
    let dateString = this.getKoreanTime(new Date());
    return gitLab.get("projects/15513334/repository/commits?since=" + dateString);
  },
  getPostTitle() {
    return gitLab.get("projects/16086191/repository/tree");
  },
  async getPostList(dir) {

    let files = [];
    let page = 1;
    let tmp
    while (page) {
      tmp = await gitLab.get("projects/16086191/repository/tree?path=" + dir + "&page=" + page++);
      if (tmp.data.length == 0) {
        console.log("FILES", files)
        for(let i = 0 ; i < files.length ; i++){
          let tt = await gitLab.get("projects/16086191/repository/files/" + dir + '%2f' +files[i].name + "/blame?ref=master");
          console.log(tt)
          files[i].date = this.getKoreanTime(new Date(tt.data[0].commit.committed_date)).split("T")[0];
        }
        return files;
      }
      files = files.concat(tmp.data);
    }

    return;
  },

  async getPost(dir) {
    //JAVA%2ftest01.md
    return gitLab.get("projects/16086191/repository/files/" + dir + "?ref=master");

    //JAVA%2ftest01.md

  }
}