import axios from 'axios'

const baseURL = "https://api.solved.ac/";
const solved = axios.create({
  baseURL: baseURL,
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  }
});

export default {
  getLevel(no) {
    return solved.get("problem_level.php?id=" + no);
  },
  getMyInfo(){
      return solved.get("user_information.php?id=ksl930425");
  }
}