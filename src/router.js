import Vue from "vue"
import VueRouter from "vue-router";
Vue.use(VueRouter);
import algorithm from "./views/algorithm";
import AboutMe from "./views/AboutMe.vue";
import HelloWorld from "./components/HelloWorld.vue";
import Post from "./views/Post.vue";
import PostList from "./views/PostList"
export default new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
  
    routes: [
        {
            path: "/algorithm",
            component: algorithm
        },
        {
            path: "/",
            component: HelloWorld
        },
        {
            path: "/aboutMe",
            component: AboutMe
        },
        {
            path: "/post/:dir/",
            component: PostList
        },
        {
            path: "/post/:dir/:md",
            component: Post
        }

    ]
})